<?php

namespace Drupal\layoutbuilder_extras_view_mode_selector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class BlockContentTypeEditForm {

  use StringTranslationTrait;

  public function alterForm(array &$form, FormStateInterface $formState) {
    $form['layoutbuilder_extras_view_mode_selector'] = [
      '#type' => 'details',
      '#title' => 'View mode icons',
      '#tree' => TRUE,
    ];

    /** @var \Drupal\block_content\BlockContentTypeForm $callbackObject */
    $callbackObject = $formState->getBuildInfo()['callback_object'];
    /** @var \Drupal\block_content\Entity\BlockContentType $blockContentType */
    $blockContentType = $callbackObject->getEntity();

    $bundle = $blockContentType->id();
    $thirdPartySettings = $blockContentType->getThirdPartySettings(
      'layoutbuilder_extras_view_mode_selector'
    );
    if (isset($thirdPartySettings['view_modes'])) {
      $thirdPartySettings = $thirdPartySettings['view_modes'];
    }

    /** @var \Drupal\layoutbuilder_extras_view_mode_selector\ViewModeSelectorHelper $viewModeSelectorHelper */
    $viewModeSelectorHelper = \Drupal::service('layoutbuilder_extras_view_mode_selector.helper');
    $viewModes = $viewModeSelectorHelper->getViewModesForBundle($bundle);

    foreach ($viewModes as $keyViewMode => $viewModeLabel) {
      $form['layoutbuilder_extras_view_mode_selector'][$keyViewMode] = [
        '#type' => 'details',
        '#title' => $viewModeLabel,
      ];
      $form['layoutbuilder_extras_view_mode_selector'][$keyViewMode]['view_mode_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#description' => $this->t('Should this be visible on the block edit form?'),
        '#default_value' => $thirdPartySettings[$keyViewMode]['view_mode_enabled'] ?? FALSE,
      ];
      $form['layoutbuilder_extras_view_mode_selector'][$keyViewMode]['view_mode_icon'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Icon path'),
        '#description' => $this->t('Relative path to the icon on the server.'),
        '#default_value' => $thirdPartySettings[$keyViewMode]['view_mode_icon'] ?? FALSE,
      ];
      $form['layoutbuilder_extras_view_mode_selector'][$keyViewMode]['view_mode_icon_alt'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Icon alt'),
        '#description' => $this->t('Alternative text for this icon.'),
        '#default_value' => $thirdPartySettings[$keyViewMode]['view_mode_icon_alt'] ?? FALSE,
      ];
    }

    // Add our own submit callback to save the values.
    array_push($form['actions']['submit']['#submit'], [$this, 'saveForm']);
  }

  /**
   * Save our custom form values to third party settings.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function saveForm(array $form, FormStateInterface $formState) {
    /** @var \Drupal\block_content\BlockContentTypeForm $callbackObject */
    $callbackObject = $formState->getBuildInfo()['callback_object'];
    /** @var \Drupal\block_content\Entity\BlockContentType $blockContentType */
    $blockContentType = $callbackObject->getEntity();

    $viewModes = $formState->getValue('layoutbuilder_extras_view_mode_selector');
    $viewModesSettings = [];
    foreach ($viewModes as $key => $viewModeConfig) {
      $viewModesSettings[$key] = [
        'view_mode_machine_name' => $key,
        'view_mode_enabled' => $viewModeConfig['view_mode_enabled'],
        'view_mode_icon' => $viewModeConfig['view_mode_icon'],
        'view_mode_icon_alt' => $viewModeConfig['view_mode_icon_alt'],
      ];
    }
    $blockContentType->setThirdPartySetting(
      'layoutbuilder_extras_view_mode_selector',
      'view_modes',
      $viewModesSettings
    );

    $blockContentType->save();
  }

}
