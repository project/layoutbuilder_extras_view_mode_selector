<?php

namespace Drupal\layoutbuilder_extras_view_mode_selector\Plugin\Block;

use Drupal\block_content\Entity\BlockContentType;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an inline block plugin type.
 *
 * @Block(
 *  id = "lb_extras_inline_block",
 *  admin_label = @Translation("Layout builder extras view mode selector: Inline block"),
 *  category = @Translation("Inline blocks"),
 *  deriver = "Drupal\layout_builder\Plugin\Derivative\InlineBlockDeriver",
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class LayoutBuilderExtrasInlineBlock extends InlineBlock {

  /** @var \Drupal\Core\Render\RendererInterface  */
  private $renderer;

  /**
   * Constructs a new InlineBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository, AccountInterface $current_user, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_display_repository, $current_user);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('current_user'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $blockContent = $form['block_form']['#block'];
    /** @var \Drupal\block_content\Entity\BlockContentType $blockContentType */
    $blockContentType = BlockContentType::load($blockContent->bundle());

    $settings = $blockContentType->getThirdPartySetting(
      'layoutbuilder_extras_view_mode_selector',
      'view_modes',
    );

    if (!$settings) {
      return $form;
    }

    $form['view_mode']['#weight'] = -50;
    // Seems rather useless.
    unset($form['view_mode']['#description']);
    $form['view_mode']['#type'] = 'radios';
    $form['view_mode']['#options'] = $this->generateRadioOptions($blockContentType, $settings, $form['view_mode']['#options']);
    $form['view_mode']['#attributes']['class'][] = 'view-mode-wrapper';
    return $form;
  }

  /**
   * @param BlockContentType $blockContentType
   *  The block entity.
   *
   * @return array
   */
  private function generateRadioOptions($blockContentType, $settings, $oldOptions) {
    $options = [];

    foreach ($oldOptions as $key => $option) {
      if (!$settings[$key]['view_mode_enabled']) {
        continue;
      }

      $render = [
        '#theme' => 'image',
        '#uri' => $settings[$key]['view_mode_icon'],
        '#alt' => $this->t($settings[$key]['view_mode_icon_alt']),
        '#title' => $settings[$key]['view_mode_machine_name'],
        '#attributes' => [
          'aria-hidden' => 'true',
        ]
      ];

      $options[$key] = $this->renderer->render($render);
      $options[$key] .= '<span class="visually-hidden">' . $settings[$key]['view_mode_machine_name'] . '</span>';
    }

    return $options;
  }

}
