<?php

namespace Drupal\layoutbuilder_extras_view_mode_selector;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;

class ViewModeSelectorHelper {

  /**
   * The Entity display repository interface.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a SchedulerManager object.
   */
  public function __construct(EntityDisplayRepositoryInterface $entityDisplayRepository) {
    $this->entityDisplayRepository = $entityDisplayRepository;
  }

  /**
   * Gets view modes for the given bundle.
   *
   * @param string $bundle
   *
   * @return array
   */
  public function getViewModesForBundle(string $bundle) {
    return $this->entityDisplayRepository->getViewModeOptionsByBundle('block_content', $bundle);
  }

}
